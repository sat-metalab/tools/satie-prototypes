# Libmapper integration example

Edu Meneses - Metalab - SAT
Dec 2021

## Setup

- Install [Satie](https://gitlab.com/sat-metalab/satie/-/blob/master/INSTALL-SATIE.md)
- Clone and compile [Libmapper](https://github.com/libmapper/libmapper/blob/main/doc/how_to_compile_and_run.md)
- Install [MapperUGen](https://github.com/libmapper/MapperUGen)
- Install [webmapper](https://github.com/libmapper/webmapper) to map signals.

Once all components are installed, both **satie_libmapper_tubie.scd** and **satie_libmapper_dustyRez.scd** can run in *headless mode*: `sclang -D satie_libmapper_tubie.scd` or `sclang -D satie_libmapper_dustyRez.scd`, respectively. It is aslo possible to run them directly on SuperCollider IDE.

You can open webmapper and use the [mapping.json](./mapping.json) file to load a mapping example for the [satie_libmapper_tubie.scd](./satie_libmapper_tubie.scd).

At that point, signals from the running SC code will be available on libmapper for mapping.
