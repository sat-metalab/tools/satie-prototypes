* Run dacTester (remotely )alongside SATIE

(you will need at least SATIE commit cf7eb3a for the code shown below to work)

  We want to start a minimal SATIE project

  #+begin_src sclang :tangle src/satie.scd :mkdirp yes
  ~satie = Satie.new(SatieConfiguration());
  ~satie.boot();
  #+end_src

  For the sake of this exercise we will also open a UV meter and look at groups and nodes being created by the SC server for some visual feedback:

  #+begin_src sclang :tangle src/satie.scd
  s.meter;
  s.plotTree;
  #+end_src

  #+begin_src sclang :tangle src/satie.scd
  ~satie.config.server.meter;
  ~satie.config.server.plotTree;
  #+end_src

  So, now, what we get is a SATIE instance in its default state and nothing else. SATIE instance has its own set of =SynthDefs= compiled but they're of no interest to us right now.

  The above code is in [[file:src/satie.scd][./src/satie.scd]]. We save it for later.

  Now, we could also ask SATIE about some details (copy-paste that into SCIDE):

  - how many audio channels have we created?
  #+begin_src sclang
  ~satie.config.server.options.numOutputBusChannels.postln;
  #+end_src

  with the above configuration, SC should respond with =2=.

  What listener format are we using?

  #+begin_src sclang
  ~satie.config.listeningFormat.postln;
  #+end_src

  SCIDE will say =[ stereoListener ]=.
  And that is exactly what we intended and expected.

  What we want to do now, is test the speakers. Remotely. We can do it via OSC but insted of talking to SATIE, we can talk directly to the =server=, that is either =scsynth= or =supernova=. With the above configuration, we are using =scsynth=, the default, as we have not specified =supernova=.

  Ok, so now we have SATIE running on a =scsynth= server but for now we want to ignore it. All we want it test the two available audio output channels. We can do it. First we define a =SynthDef= that will play some white noise:

  #+begin_src sclang :tangle src/satie.scd
  SynthDef(\dacTest, { |out, gate=1|
      var env = EnvGen.kr(Env.asr(0.03, 0.3, 0.01), gate, doneAction: Done.freeSelf);
      var sig = WhiteNoise.ar * env;
      Out.ar(out, sig)
  }).add;
  #+end_src


  and instead of instantiating it with =sclang= we will do it via OSC from CLI!

  #+begin_src bash
  oscsend localhost 57110 /s_new siiisi dacTest 5000 0 1 out 0
  #+end_src

  #+RESULTS:

  You should hear some white noise in your left ear.
  let's kill it

  #+begin_src shell
  oscsend localhost 57110 /n_set isi 5000 gate 0
  #+end_src

  #+RESULTS:

  So, let's now write a DAC tester for our stereo listener:

  #+begin_src shell :tangle src/dacTest.sh
  oscsend localhost 57110 /s_new siiisi dacTest 5000 0 1 out 0
  sleep 1
  oscsend localhost 57110 /n_set isi 5000 gate 0
  oscsend localhost 57110 /s_new siiisi dacTest 5001 0 1 out 1
  #+end_src

  #+RESULTS:


  now, if we want to put it all together:

  - you will need to quit your SCIDE and make sure =sclang= and =scsynth= are not running.

  - Open a terminal in this directory and run:
  #+begin_src shell
  sclang src/satie.scd
  #+end_src

  - open a new frame in your terminal (or a new terminal) and run:
  #+begin_src shell
  bash src/dacTester.sh
  #+end_src

  You should have seen and heard noise in channel left and right...

  Thank you for reading.
