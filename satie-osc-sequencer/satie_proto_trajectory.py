#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created between Feb 15-27 2021

@author: Marc Lavallée <mlavallee@sat.qc.ca>


Prototype trajectory builder for SATIE.

It implements a trajectory_loop function
to make interpolated loops from target points;
as an option, it makes sure the trajectory end is the same than its beginning,
for seamless looping.

It's main function defines a sound to play,
generates random XYZ trajectories for this sound (with plots)
and waits for playing commands from the console.

It uses the SatieProtoSequencer and the SatieOSC classes

This prototype is maint to run in a ipython3 terminal
(if ran directly, it will exit without playing)
from ipython3: "run satie_proto_trajectory.py"
then use "mq.put()" to send messages ('play', 'pause', 'loop on', ...)
typical use: mq.put('loop on'), mq.put('play'), mq.put('pause')

the sequencer is immune to the ctrl-c signal (keyboard interruption)
so interactive commands can be sent while the sequencer is still running.
ex; when using plugins like 'zkarpluck1', trigger signals can be sent using:
satieosc.send('/satie/source/set', 'zkarpluck1Ambi3_0', 't_trig', 1)
this command can be repeated from an infinite loop and stopped with ctrl-c

"""

# import sys
import atexit

import random
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import matplotlib._color_data as mcd
from multiprocessing import Queue
from satie_osc import SatieOSC
from satie_proto_sequencer import SatieProtoSequencer

def trajectory_loop(targets, duration=1, resolution=0.01, interpolation_type="quadratic"):

    targets_copies = np.concatenate((targets, targets, targets, [targets[0]]))

    targets_copies_timeline = np.linspace(-duration,
                                          2 * duration, 3 * len(targets) + 1)

    # interpolate function for copies of targets
    interpolation_fcn = interpolate.interp1d(
        targets_copies_timeline, targets_copies, kind=interpolation_type
    )

    # interpolation timeline
    interpolation_timelime = np.linspace(
        -duration, 2 * duration, int(3 * duration / resolution)
    )

    # interpolate points
    interpolated = interpolation_fcn(interpolation_timelime)

    # get center sections (from 0 to duration)
    # including timeline for plotting
    start = int(targets_copies.shape[0] / 3)
    targets_tuple = (
        targets_copies_timeline[start: 2 * start],
        targets_copies[start: 2 * start],
    )
    start = int(interpolated.shape[0] / 3)
    interpolated_tuple = (
        interpolation_timelime[start: 2 * start],
        interpolated[start: 2 * start],
    )

    return (targets_tuple, interpolated_tuple)


def main():

    # define one localhost instance of SatieOSC
    satieosc = SatieOSC(satie_host="localhost")

    synthdefs = []

    min_targets = 3
    max_targets = 11

    min_duration = 5
    max_duration = 20

    duration = min_duration + max_duration * random.random()

    resolution = 0.01

    # generate and plot trajectories

    nx = random.randint(min_targets, max_targets)
    tx = 2 * np.random.random_sample(nx) - 1
    targets_x, interpolated_x = trajectory_loop(tx, duration, resolution)

    px = interpolated_x[1]

    ny = random.randint(min_targets, max_targets)
    ty = 2 * np.random.random_sample(ny) - 1
    targets_y, interpolated_y = trajectory_loop(ty, duration, resolution)

    py = interpolated_y[1]

    nz = random.randint(min_targets, max_targets)
    tz = 2 * np.random.random_sample(nz) - 1
    targets_z, interpolated_z = trajectory_loop(tz, duration, resolution)
    pz = interpolated_z[1]

    # array of series of cartesian points
    sequencing_data = np.array(
        [
            [
                px,
            ],
            [
                py,
            ],
            [
                pz,
            ],
        ]
    )

    # plugin = "sawAmbi3"
    # ~ plugin = 'zkarpluck1Ambi3'
    plugin = "testeeAmbi3"

    num_sources = sequencing_data.shape[1]
    for source_num in range(num_sources):
        synthdef = f"{plugin}_{source_num}"
        synthdefs.append(synthdef)
        satieosc.send("/satie/scene/createSource", synthdef, plugin)

    mq = Queue()
    seq = SatieProtoSequencer(
        mq, satieosc, synthdefs, sequencing_data,
        distance_scaling=1, resolution=resolution
    )
    seq.start()

    def exit_handler():
        for synthdef in synthdefs:
            satieosc.send("/satie/scene/deleteNode", synthdef)
        seq.stop()

    atexit.register(exit_handler)

    # colors for plotting trajectories
    cx = mcd.XKCD_COLORS["xkcd:green"]
    cy = mcd.XKCD_COLORS["xkcd:blue"]
    cz = mcd.XKCD_COLORS["xkcd:red"]

    plt.plot(targets_x[0], targets_x[1], "o", label="X", color=cx)
    plt.plot(interpolated_x[0], interpolated_x[1], color=cx)

    plt.plot(targets_y[0], targets_y[1], "o", label="Y", color=cy)
    plt.plot(interpolated_y[0], interpolated_y[1], color=cy)

    plt.plot(targets_z[0], targets_z[1], "o", label="Z", color=cz)
    plt.plot(interpolated_z[0], interpolated_z[1], color=cz)

    plt.legend()
    plt.show(block=False)

    return mq

if __name__ == "__main__":
    mq = main()

# end
