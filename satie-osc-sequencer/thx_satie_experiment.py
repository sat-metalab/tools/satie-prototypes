#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created between Feb 15-27 2021

@author: Marc Lavallée <mlavallee@sat.qc.ca>


Experimental spatialisation of a THX Logo Theme approximation, based on:
    https://musicinformationretrieval.com/thx_logo_theme.html

This file implements interpolation functions for frequencies and distances,
and functions to generate positions derived from the frequencies and amplitudes.

The positions are following a spiral path on top of a unit sphere,
from bottom to top, following the frequencies.

Log scales are used for interpolations.

The main function will:

- use a make_thx_voices function to generate the THX parameters and plot them.
- plot 3d graphs of the start and end positions of the notes
- interpolate frequencies and positions
- assemble sequence data
- start sequencer
- wait for start command

It must be used in a iPython console;
- start ipython and type: "run thx_satie_experiment.py"
- start sequence with: "mq.put('play')"

SATIE must already be running on the same computer, waiting for commands.

"""

# import sys
import atexit
import math
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from multiprocessing import Queue
from thx_logo_theme import make_thx_voices
from satie_osc import SatieOSC
from satie_proto_sequencer import SatieProtoSequencer
import logging
logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.basicConfig(level=logging.DEBUG)

# flag to enable plotting
PLOT=True

# delay between OSC messages in seconds
# (its inverse would be the OSC "sampling rate")
RESOLUTION = 0.05

# generator for all of the THX notes; it should normally produce a continuous sound
PLUGIN = "saw"

# set to "Ambi3" if using 3rd order HOA Binaural spatialisation
# or to "" for normal spatializers
PLUGINTYPE = "Ambi3"


def interpolate_thx(targets, duration=1, resolution=RESOLUTION, interpolation_type="slinear"):

    targets_timeline = np.linspace(0, duration, len(targets))

    # interpolate function for copies of targets
    interpolation_fcn = interpolate.interp1d(
        targets_timeline, targets, kind=interpolation_type
    )

    # interpolation timeline
    interpolation_timelime = np.linspace(
        0, duration, int(duration / resolution)
    )

    # including timeline for plotting
    targets_tuple = (
        targets_timeline,
        targets
    )

    # interpolate points
    interpolated_tuple = (
        interpolation_timelime,
        interpolation_fcn(interpolation_timelime)
    )

    return (targets_tuple, interpolated_tuple)


def polar_z_spiral(z=0, z_min=-1, z_max=1, is_log=False, z_range=1, turns=1, az=0, el=0):
    """return azimuth and elevation of a vector on a unit sphere
    for a vertical position between a min and a max value"""

    if turns < 0:
        turns = 0

    if is_log:
        if z_min < 1:
            z_min = 1
        if z < 1:
            z = 1
        z = math.log(z)
        z_min = math.log(z_min)
        z_max = math.log(z_max)

    if z_max <= z_min:
        return 0, 0

    ratio = (z-z_min) / (z_max-z_min)
    pos = -1.0 + 2.0 * ratio

    elevation = el + z_range * pos * 90
    if elevation < -90:
        elevation = -90
    if elevation > 90:
        elevation = 90

    azimuth = az + (pos * 180) * turns % 360
    if azimuth > 180:
        azimuth = azimuth-360

    return azimuth, elevation


def sph2xyz(azel, distance=1):
    az = math.radians(azel[0])
    el = -math.radians(azel[1]-90)
    x = distance * math.sin(el) * math.cos(az)
    y = distance * math.sin(el) * math.sin(az)
    z = distance * math.cos(el)
    return x, y, z


def cart_z_spiral(*args, **kwargs):
    return sph2xyz(polar_z_spiral(*args, **kwargs))


if True:
# def main(PLOT=False, RESOLUTION=0.05, PLUGIN="saw", PLUGINTYPE="Ambi3"):

    voices, amplitude = make_thx_voices(cents=[2, 7], plot=PLOT)

    # plot spatial sustain spatial positions
    if PLOT:
        nb=14
        for z_range in [0.05, 0.5]:
            fig=plt.figure()
            ax=fig.add_subplot(111, projection='3d')
            u=np.linspace(0, 2 * np.pi, nb)
            v=np.linspace(0, np.pi, nb)
            x=0.98 * np.outer(np.cos(u), np.sin(v))
            y=0.98 * np.outer(np.sin(u), np.sin(v))
            z=0.98 * np.outer(np.ones(np.size(u)), np.cos(v))
            elev=10.0
            ax.plot_surface(x, y, z, rstride=1, cstride=1,
                            color='y', linewidth=0, alpha=0.5)
            ax.view_init(elev=elev, azim=0)

            for i in range(nb):
                z = math.exp((i+1)/nb)
                z_min = math.exp(1/nb)
                z_max = math.exp(1)
                az, el= polar_z_spiral(z=z, z_min=z_min, z_max=z_max,
                                       is_log=True, turns=3*z_range, z_range=z_range)
                x, y, z = sph2xyz((az, el))
                ax.plot(x, y, z, color='r', marker='o')


    # interpolate voices
    ivoices = []
    for voice in voices:
        targets, interpolated = interpolate_thx(np.log(voice),
                                                interpolation_type="slinear",
                                                duration=15, resolution=RESOLUTION)
        ivoices.append((targets, interpolated))

    # frequencies as log values
    frequencies = []
    for targets, ivoice in ivoices:
        frequencies.append(np.exp(ivoice[1]))

    # number of voices (42)
    num_voices = len(frequencies)
    # number of updates (depends on RESOLUTION)
    num_updates = len(frequencies[0])

    # calc amplitudes
    targets, amplitudes = interpolate_thx(amplitude,
                                            interpolation_type="slinear",
                                            duration=15, resolution=RESOLUTION)
    # calc distances
    distances = num_voices * [1/amplitudes[1]]

    # final frequency range, used to limit the orientation range
    f_min = math.log(min(voices[:, -1]))
    f_max = math.log(max(voices[:, -1]))

    # calc orientations

    # azimuths = num_voices * [num_updates*[0]]
    azimuths = []
    # elevations = num_voices * [num_updates*[0]]
    elevations = []

    for nv in range(num_voices):
        ivoice = ivoices[nv]
        azimuths.append([])
        azimuth = azimuths[-1]
        elevations.append([])
        elevation = elevations[-1]

        for nu in range(num_updates):
            freq = ivoice[1][1][nu]
            amp = amplitudes[1][nu]
            z_range = 0.5*amp
            az, el = polar_z_spiral(az=0, z_range=0.5*amp,
                z=freq, z_min=f_min, z_max=f_max,
                is_log=True, turns=3*z_range)
            azimuth.append(az)
            elevation.append(el)

    position_data = np.array(
        [
            azimuths,
            elevations,
            distances,
        ]
    )

    synth_params = ['frequency']
    synthdef_data = np.array(
        [
            frequencies,
        ]
    )

    synthdefs = []

    satieosc = SatieOSC(satie_host="localhost")

    num_sources = position_data.shape[1]
    for source_num in range(num_sources):
        synthdef = f"{PLUGIN}_{source_num}"
        synthdefs.append(synthdef)
        satieosc.send("/satie/scene/createSource", synthdef, PLUGIN+PLUGINTYPE)
        satieosc.send("/satie/source/state", synthdef, 0)
        satieosc.send(
                "/satie/source/set", synthdef,
                'frequency', frequencies[source_num][0],
                'cutoff_freq', 3000
            )
        satieosc.send(
            "/satie/source/update", synthdef,
            0, 0, -99, 30, 15000
        )

    mq = Queue()
    seq = SatieProtoSequencer(
        mq, satieosc, synthdefs, position_data, synthdef_data, synth_params=synth_params,
        is_spherical=True, distance_scaling=2, resolution=RESOLUTION
    )

    seq.start()

    def exit_handler():
        for synthdef in synthdefs:
            satieosc.send("/satie/scene/deleteNode", synthdef)
        seq.stop()
    atexit.register(exit_handler)

#     return mq

# if __name__ == "__main__":
#     mq = main(PLOT=True, RESOLUTION=0.05, PLUGIN="saw", PLUGINTYPE="Ambi3")
#     print("start sequence with: mq.put('play')")
