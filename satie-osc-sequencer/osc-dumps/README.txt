This folder contains sequences of OSC messages captured using oscdump 
from the liblo library.

To play back a sequence: 

oscsendfile "osc.udp://localhost:18032" <file.osc>
