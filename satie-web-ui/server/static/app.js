import { html, render, Component } from 'https://unpkg.com/htm/preact/index.mjs?module'
import * as socketio from 'https://jspm.dev/socket.io-client@2.3.0'

const SERVER_ADDR = 'http://' + document.domain + ':' + location.port;

// socket.io hack when imported from jspm.dev
// here, socketio.default is basically the io() function
// console.log(socketio);
const socket = socketio.default(SERVER_ADDR);

export default class App extends Component {
    constructor() {
        super();
        this.state = { isConnected: false };
    }

    handleClick() {
        const isConnected = this.state.isConnected;
        if (isConnected) {
            this.setState({ isConnected: false });
            socket.emit('disconnect satie');
        } else {
            this.setState({ isConnected: true });
            socket.emit('initialize satie');
        }
    }

    render() {
        return html`
            <div>
                <button onClick=${() => this.handleClick()}>
                    ${this.state.isConnected ? 'Disconnect' : 'Connect'}
                </button>
            </div>
       `;
    }
}

export class Query extends Component {
    constructor() {
        super();
        this.plugins;
        this.state = { showList: true };

        socket.on('plugins updated', (names) => {
            this.plugins = this.handlePluginList(names);
            this.setState({ showList: false });
        });
    }

    handleQuery() {
        socket.emit('query');
    }

    handlePluginList(names) {
        const plugins = names.map((name) =>
            html`<li>${name}</li>`
        );
        return html`<ul>${plugins}</ul>`;
    }

    render() {
        return html`
            <div>
                <button onClick=${() => this.handleQuery()}>
                    Query Source Plugins
                </button>
                <div class="plugin-list" hidden=${this.state.showList}>
                    ${this.plugins}
                </div>
            </div>
        `;
    }
}

function PlayButton(props) {
    return html`<button onClick=${props.onClick}>${props.name}</button>`;
}

export class Player extends Component {
    constructor() {
        super();
        this.state = { isPlaying: false };
    }

    handleOnPlay() {
        if (this.state.isPlaying === false) {
            this.setState({ isPlaying: true });
            socket.emit('play');
        }
    }

    handleOnStop() {
        if (this.state.isPlaying === true) {
            this.setState({ isPlaying: false });
            socket.emit('stop');
        }
    }

    render() {
        const isPlaying = this.state.isPlaying;
        let player;
        if (isPlaying) {
            player = html`
                <${PlayButton} name="Stop" onClick=${() => this.handleOnStop()} />
                <${Parameters} />
            `;
        } else {
            player = html`
                <${PlayButton} name="Play Dust" onClick=${() => this.handleOnPlay()} />
            `;
        }

        return html`
            <div>
                ${player}
            </div>
        `;
    }
}

function ValueSlider(props) {
    return html`
        <input type="range" id=${props.name} name=${props.name} min=${props.min} max=${props.max} step=${props.step} oninput=${props.oninput}></input>
        <label for=${props.name}>${props.name}</label>
    `;
}

export class Parameters extends Component {
    constructor() {
        super();
    }

    handleValueChanged(event) {
        const id = event.target.id;
        const value = event.target.value;
        socket.emit('parameter set', id, value);
    }

    render() {
        // DustDust plugin has three parameters: density1, density2, and trimDB
        let density1 = html`<${ValueSlider} id="density1" name="density1" min="1.0" max="10.0" step="0.01" oninput=${event => this.handleValueChanged(event)} />`;
        let density2 = html`<${ValueSlider} id="density2" name="density2" min="10.0" max="80.0" step="0.1" oninput=${event => this.handleValueChanged(event)} />`;
        // fake a gain trim control using a linear -12 to 0 range
        let trim = html`<${ValueSlider} id="trimDB" name="trimDB" min="-12.0" max="0.0" step="0.1" oninput=${event => this.handleValueChanged(event)} />`;
        return html`
            <div>${density1}</div>
            <div>${density2}</div>
            <div>${trim}</div>
        `;
    }
}

render(
    html`
        <h3>🤖🤖🤖 __-=| SATIE |=-__ 🤖🤖🤖</h3>
        <div class="connect"><${App} /></div>
        <div class="query"><${Query} /></div>
        <div class="player"><${Player} /></div>
    `,
    document.getElementById('app-satie-ui')
);
