import os

from flask import Flask, render_template
from .events import socketio


# this function is taken almost verbatim from:
#     https://flask.palletsprojects.com/en/1.1.x/tutorial/factory/#the-application-factory
# the instance directory and config.py features below aren't used in this prototype
def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # init socketio to use app
    socketio.init_app(app)

    # serve index.html
    @app.route('/')
    def index():
        return render_template('index.html')

    return app
