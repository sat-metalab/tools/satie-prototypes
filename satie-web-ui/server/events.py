from flask_socketio import SocketIO
from pysatie.satie import Satie

# create a basic SocketIO
# we will pass it an App later using init_app()
socketio = SocketIO()

# create an instance of Satie
satie = Satie()


def update_plugins_list(sender):
    satie.on_plugin_list_updated.disconnect(update_plugins_list)
    names = []
    for plugin in satie.plugins:
        names.append(plugin.name)
    socketio.emit('plugins updated', names)


@socketio.on('initialize satie')
def handle_initialize():
    satie.initialize()


@socketio.on('disconnect satie')
def handle_initialize():
    satie.disconnect()


@socketio.on('query')
def handle_query():
    satie.on_plugin_list_updated.connect(update_plugins_list)
    satie.query_audiosources()


@socketio.on('play')
def handle_play():
    dust = satie.get_plugin_by_name('DustDust')
    satie.create_source_node('dustey', dust, group="default", gainDB=-6)


@socketio.on('stop')
def handle_stop():
    satie.delete_node('dustey')


@socketio.on('parameter set')
def handle_parameter_set(name, value):
    satie.node_set('dustey', name, float(value))
