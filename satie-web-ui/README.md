A prototype web interface for controlling SATIE.

- SATIE: https://gitlab.com/sat-metalab/SATIE
- PySATIE: https://gitlab.com/sat-metalab/PySATIE
- Flask: https://flask.palletsprojects.com
- Flask-SocketIO: https://flask-socketio.readthedocs.io
- Socket.io: https://socket.io
- Preact: https://preactjs.com
- HTM: https://github.com/developit/htm

## installation

### create Python virtualenv and active it

```bash
python3 -m venv venv
. venv/bin/active
```

### install dependencies using requirements.txt

First, install wheel:

```bash
pip3 install wheel
```

Now, you should be able to install the dependencies, with the exception of PySATIE, by using the `requirements.txt` file:

```bash
pip3 install -r requirements.txt
```

### install PySATIE

To install PySATIE, clone the project's repo somewhere on your system.
You'll need to use a specific branch called `feat/signal`.

```bash
cd ~/git
git clone https://gitlab.com/sat-metalab/PySATIE.git
cd PySATIE
git checkout feat/signal
```

Making sure you are still inside your virtual environment, install PySATIE:

```bash
pip3 install -e .
```

## run the project

Flask uses environment variables. Set these and run the server via `flask run`.

```bash
export FLASK_APP=server
export FLASK_ENV=development
flask run
```

The server will serve the page at `http://127.0.0.1:5000/`.
Before you can do anything, you need to have SATIE up and running on your system.
A very basic SATIE project will do the trick.

```supercollider
(
a = Satie.new;
a.debug = true;
a.boot;
)

a.quit;
```

First, click the `Connect` button, followed by `Query Source Plugins`.
Then, click `Play Dust` to instantiate a synth in SATIE and control its parameters.
