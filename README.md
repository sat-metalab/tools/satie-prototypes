# SATIE prototypes

This is a collection of some SATIE examples and prototypes, possibly involving other tools.

Here's what you can find here at the moment:

- [remote DAC tester](./remoteDACtester) - an illustration of how to conduct the DAC tester in SATIE from Python
- [interacting with SATIE via Jupyter (Python) Notebook](./SATIE_jupyter)
- [SATIE OSC sequencer in Python](./satie-osc-sequencer)
- [LivePose->Chataigne->SATIE](./LivePose-Chataigne-SATIE) - using [LivePose](https://gitlab.com/sat-metalab/livepose/) with [Chataigne](https://benjamin.kuperberg.fr/chataigne/en#home) to control SATIE
- [LivePose->libmapper->SATIE](./LivePose-libmapper-SATIE) - using [LivePose](https://gitlab.com/sat-metalab/livepose/) with [libmapper](http://libmapper.github.io/) to control SATIE
- [LivePose->OSSIA->SATIE](./LivePose-OSSIA-SATIE) - using [LivePose](https://gitlab.com/sat-metalab/livepose/) with [ossia](https://ossia.io/) to control SATIE
