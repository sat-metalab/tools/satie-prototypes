// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin


The parameters dX, dY, lX and lY correspond to the cartesians coordinates of the AudioDice (d) and of the listener (l).
This plugin is used for the BeamTracer with the plugins sndFileLW and sndFileRW.


            dY    lY
      _________________________ > y
     |      .     .  Right Wall
     |      .     .
 dX  |. . . d     .
     |            .
     |            .
 lX  |. . . . . . l
     |
     |
     |
     | Left Wall
     |
     V
     x




*/

~name = \sndInDirect;
~description = "Stream a soundfile from disk for the direct beam for the BeamTracer";
~channelLayout = \mono;

~function = { |bus = 0, gaindB=10, distDdirL=1, distDRWL=1.5, distDLWL=2|
	var output, input, delay;

	delay = distDLWL-distDdirL+amclip(1,distDRWL-distDLWL);
	input = SoundIn.ar(bus);
	gaindB.dbamp * DelayN.ar(input,1,delay.abs/344);

};

























