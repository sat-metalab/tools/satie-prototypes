# Satie AudioDice

## Setup

To use this spatializer you have to :
- Install [Satie](https://gitlab.com/sat-metalab/satie/-/blob/master/INSTALL-SATIE.md)
- Install [Sparta](https://leomccormack.github.io/sparta-site/) to decode ambisonic
- Install [VSTPlugin](https://github.com/Spacechild1/vstplugin) to control the Sparta plugin


## How does it work

We'd like to integrate a spatializer that would improve the precision of directivity as well as the fluidity of source movement. For this, we're inspired by the work of Franz Zotter, who created the IKO [1], which like the AudioDices are compact spherical loudspeaker systems, but with 20 transducers instead of 12.

This spatializer uses two ambisonic decoders in parallel: AllRAD (panning) decoding for high frequencies, to ensure precise directivity, and pseudo-inverse (beamforming) decoding for low frequencies, to ensure smooth source movement. Due to the reduced number of loudspeakers, it is not necessary to use an ambisonic order higher than the second order.

In addition to the two ambisonic decoders, the spatializer incorporates various filters and EQs, as shown in the figure below.

(the dashed line blocks are not integrated yet)

![pipeline](.\images\Pipeline.png)



## Decoding pipeline

The spatialization steps are described below: 

- CrossOver: the signal is split in two using a Linkwitz-Riley filter. High frequencies are sent through the panning band, while low frequencies are sent through the beamforming band.

- AllRAD: This is a simple 2nd-order AllRad ambisonic panning decoder for the Sparta vst.

- Group Delay: By adding a latency to the panning band, signals from both branches will arrive in phase.

- Radiation Control Filter [3]: This filter attenuates the undesirable lobes of the various spherical harmonics using several types of filter, such as All-Pass filters, which invert the phase without modifying the amplitude. To integrate this filter, I decided to use the supercollider's pvcalc function, which lets you read and modify the amplitudes and phases of a signal's frequency response in real time, but it's also possible to integrate this function using an impulse response.

- Ambisonics Decoder: This is a simple 2nd-order pseudo-inverse beamforming ambisonics decoder for the Sparta vst.

- Crosstalk EQ: As the system is a network of loudspeakers sharing the same structure, there is a strong vibro-acoustic coupling between them. This is why we need to integrate an EQ that limits this coupling as much as possible. However, anechoic room measurements of the vibration of the membranes when a sine sweep is played on one of the transducers are required to deduce the EQ to be applied to cancel the effect of this coupling. These measurements have not yet been carried out for AudioDices, so this EQ is not yet available.

- On-axis beam equalization [4] [5]: This EQ restores the timbre and coloration of the original sound that has been modified by the various stages of spatialization. To find this EQ, simply follow the algorithm proposed by Behrends [5], which determines which impulse response to apply according to a given frequency response. This function has not yet been integrated due to lack of time.

A first prototype of this spatializer has been created for AudioDices, although spatialization is missing 3 steps. In addition, it would seem advisable to apply the various filters not by supercollider functions but rather by convolution, as is the case with the IKO spatializer in particular.

## Bibliography

[1] Stefan Riedel, Franz Zotter; Design, Control, and Evaluation of Mixed-Order, Compact Spherical Loudspeaker Arrays. Computer Music Journal 2020; 44 (4): 60–76

[2] Zotter, Franz & Zaunschirm, Markus & Frank, Matthias & Kronlachner, Matthias. (2017). A Beamformer to Play with Wall Reflections: The Icosahedral Loudspeaker. Computer Music Journal. 41. 50-68. 10.1162/comj_a_00429. 

[3] Zotter, Franz & Frank, Matthias. (2019). Ambisonics: A Practical 3D Audio Theory for Recording, Studio Production, Sound Reinforcement, and Virtual Reality. 10.1007/978-3-030-17207-7. 

[4] Frank, Matthias & Gölles, Lukas & Riedel, Stefan & Zotter, Franz. (2023). Equalizing the coloration of different Ambisonic order weightings. 

[5] Behrends, Herwig & von dem Knesebeck, Adrian & Bradinal, Werner & Neumann, Peter & Zölzer, Udo. (2011). Automatic Equalization Using Parametric IIR Filters. Journal of the Audio Engineering Society. Audio Engineering Society. 59. 102-109. 



